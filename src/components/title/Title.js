import React from "react";
import TitleImage from "./title-image/TitleImage";
import TitleText from "./title-text/TitleText";

const Title = () => {
    return (
        <React.Fragment>
            <TitleText />
            <TitleImage />
        </React.Fragment>
    )
}
export default Title;