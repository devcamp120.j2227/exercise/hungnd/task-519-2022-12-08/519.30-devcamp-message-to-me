
import { Row } from "reactstrap"
import { Col } from "reactstrap"

const TitleText = () => {
    return (
        <Row >
            <Col className="text-center">
                <h1>Chào mừng đến với Devcamp 120</h1>
            </Col>
        </Row>
    )
}

export default TitleText;