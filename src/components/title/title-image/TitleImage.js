
import { Row, Col } from "reactstrap"

import background from "../../../assets/images/background.jpg";

const TitleImage = () => {
    return (
        <Row className="mt-3">
            <Col className="text-center">
                <img src={background} style={{ width: "500px" }} alt="Background" />
            </Col>
        </Row>
    )
}

export default TitleImage;