import { useState } from "react";
import InputMessage from "./body-input/InputMessage";
import LikeImage from "./body-output/LikeImage";

const Body = () => {
    const [inputMessage, setInputMessage] = useState("");
    const [outputMessage, setOutputMessage] = useState([]);
    const [likeImg, setLikeDisplay] = useState(false);


    //tạo 1 hàm cho phép thay đổi state inputMessage dựa vào chuỗi truyền vào
    const inputMessageChangeHandler = (value) => {
        setInputMessage(value);
    }
    //tạo 1 hàm cho phép thay đổi state outputMessage và likeImg
    const outputMessageChangeHandler = () => {
        if (inputMessage) {
            setOutputMessage([...outputMessage, inputMessage]);
            setLikeDisplay(true);
        }
    }
    return (
        <>
            <InputMessage inputMessageProp={inputMessage} inputMessageChangeHandlerProp={inputMessageChangeHandler} outputMessageChangeHandlerProp={outputMessageChangeHandler} />
            <LikeImage outputMessageProp={outputMessage} likeImgProp={likeImg} />
        </>
    )
}


export default Body;