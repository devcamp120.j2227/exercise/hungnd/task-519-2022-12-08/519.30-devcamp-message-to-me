import { Button, Col, Input, Label, Row, FormGroup} from "reactstrap";

const InputMessage = ({ inputMessageProps, inputMessageChangeHandlerProp, outputMessageChangeHandlerProp }) => {
    const onBtnClickHandler = () => {
        console.log("Nút đã được bấm");
        outputMessageChangeHandlerProp();
    }

    const onInputChangeHandler = (event) => {
        console.log("Input đã được nhập");
        console.log("Giá trị:", event.target.value);
        inputMessageChangeHandlerProp(event.target.value);
    }
    return (
        <>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    <Label for="inputMessage">
                        Mesage cho bạn 12 tháng tới
                    </Label>
                </Col>
            </Row>
            <Row className='mt-3'>
                <Col md={{
                    offset: 3,
                    size: 6
                }}
                    sm="12">
                    <FormGroup floating>
                        <Input
                            id="message"
                            name="message"
                            placeholder="Nhập vào message"
                            type="text"
                            onChange={onInputChangeHandler}
                            value={inputMessageProps}
                        />
                        <Label for="message">
                        Nhập vào message
                        </Label>
                    </FormGroup>
                    
                </Col>
            </Row>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    <Button color="primary" onClick={onBtnClickHandler}>Gửi thông điệp</Button>
                </Col>
            </Row>
        </>
    )
}
export default InputMessage;